#
msgid ""
msgstr ""
"PO-Revision-Date: 2023-05-17 03:10+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Urdu <https://hosted.weblate.org/projects/censorship-no/"
"client-front-end/ur/>\n"
"Language: ur\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.18-dev\n"

#: client/front-end.md:block 1 (header)
msgid "Annex: The Ouinet client front-end"
msgstr "ملحقہ: Ouinet کلائنٹ فرنٹ اینڈ"

#: client/front-end.md:block 2 (paragraph)
msgid ""
"The Ouinet client (as run by e.g. Ceno Browser) offers a front-end page with "
"some information and actions which may be useful for debugging the client. "
"Many of them are also offered by the Ceno Extension via the [*Ceno Settings* "
"page](../browser/settings.md), though others are only available here."
msgstr ""
"Ouinet کلائنٹ (جیسا کہ Ceno براؤزر چلاتا ہے) کچھ معلومات اور اعمال کے ساتھ "
"ایک فرنٹ اینڈ صفحہ پیش کرتا ہے جو کلائنٹ کو ڈیبگ کرنے کے لیے مفید ہو سکتا "
"ہے۔ ان میں سے بہت سے Ceno ایکسٹینشن کے ذریعہ [*Ceno ترتیبات* صفحہ](../"
"browser/settings.md) کے ذریعے بھی پیش کیے جاتے ہیں، حالانکہ دیگر صرف یہاں "
"دستیاب ہیں۔"

#: client/front-end.md:block 3 (paragraph)
msgid ""
"The front-end is accessible using any plain Web browser running on the same "
"device (you can use Ceno too). Its default address is <http://127.0.0.1:8078/"
">. If you open it, you will see something like the figure below."
msgstr ""
"فرنٹ اینڈ ایک ہی ڈیوائس پر چلنے والے کسی بھی سادہ ویب براؤزر کا استعمال کرتے "
"ہوئے قابل رسائی ہے (آپ Ceno بھی استعمال کر سکتے ہیں)۔ اس کا ڈیفالٹ پتہ "
"<http://127.0.0.1:8078/> ہے۔ اگر آپ اسے کھولیں گے تو آپ کو نیچے دی گئی تصویر "
"کی طرح کچھ نظر آئے گا۔"

#: client/front-end.md:block 4 (paragraph)
msgid "![Figure: The client front-end](images/front-end.png)"
msgstr "![تصویر: کلائنٹ فرنٹ اینڈ](images/front-end.png)"

#: client/front-end.md:block 5 (paragraph)
msgid "The items shown in the page include:"
msgstr "صفحہ میں دکھائے گئے آئٹمز میں شامل ہیں:"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"A link to enable the client as a certificate authority (CA) at your browser, "
"since the client needs to intercept HTTPS traffic."
msgstr ""
"آپ کے براؤزر پر کلائنٹ کو بطور سرٹیفکیٹ اتھارٹی (CA) فعال کرنے کا ایک لنک، "
"کیونکہ کلائنٹ کو HTTPS ٹریفک کو روکنے کی ضرورت ہے۔"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"You only need this to use a plain browser for testing the Ouinet client, in "
"which case you will also have to configure its HTTP/HTTPS proxies to "
"`127.0.0.1:8077`, and manually enable the [Ceno Extension][Ceno Extension] "
"for injection to work. We very strongly recommend using a *separate, "
"specific browser profile* for this purpose."
msgstr ""
"آپ کو صرف Ouinet کلائنٹ کی جانچ کے لیے ایک سادہ براؤزر استعمال کرنے کے لیے "
"اس کی ضرورت ہے، ایسی صورت میں آپ کو اس کی HTTP/HTTPS پراکسیز کو "
"`127.0.0.1:8077` پر ترتیب کرنا پڑے گا، اور انجکشن کو کام کرنے کے لئے [Ceno "
"ایکسٹینشن][Ceno Extension] کو دستی طور پر فعال کرنا ہوگا۔ ہم اس مقصد کے لیے "
"ایک *علیحدہ، مخصوص براؤزر پروفائل* استعمال کرنے کی سختی سے سفارش کرتے ہیں۔"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Please note that none of this needs to be done for Ceno Browser, since it is "
"already configured like that."
msgstr ""
"براہ کرم نوٹ کریں کہ Ceno براؤزر کے لیے اس میں سے کسی کو بھی کرنے کی ضرورت "
"نہیں ہے، کیونکہ یہ پہلے ہی اس طرح ترتیب دیا گیا ہے۔"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Buttons to enable or disable the different mechanisms used by the client to "
"access content."
msgstr ""
"مواد تک رسائی کے لیے کلائنٹ کے استعمال کردہ مختلف طریقہ کار کو فعال یا غیر "
"فعال کرنے کے بٹن۔"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Selectors to choose different log levels, like the default `INFO` "
"(informational messages, warnings and errors) or `DEBUG` (verbose output "
"useful for reporting errors). The log file can also be enabled and retrieved "
"from here."
msgstr ""
"منتخب کنندگان مختلف لاگ لیولز کا انتخاب کریں، جیسے ڈیفالٹ `INFO` (معلوماتی "
"پیغامات، انتباہات اور غلطیاں) یا `DEBUG` (غلطیوں کی اطلاع دینے کے لیے مفید "
"وربوز آؤٹ پٹ)۔ لاگ فائل کو یہاں سے بھی فعال اور بازیافت کیا جا سکتا ہے۔"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"When enabling the log file, the log level is automatically set to `DEBUG` "
"(though you may change it again from here). When disabling the log file, the "
"original log level is restored."
msgstr ""
"لاگ فائل کو فعال کرتے وقت، لاگ لیول خود بخود `DEBUG` پر سیٹ ہو جاتا ہے "
"(حالانکہ آپ اسے یہاں سے دوبارہ تبدیل کر سکتے ہیں)۔ لاگ فائل کو غیر فعال کرتے "
"وقت، لاگ کی اصل سطح بحال ہو جاتی ہے۔"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Global client state and version information. Useful when reporting errors."
msgstr ""
"عالمی کلائنٹ کی حالت اور ورژن کی معلومات۔ غلطیوں کی اطلاع دیتے وقت مفید ہے۔"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Information about client connectivity and injector addressing. The default "
"`bep5` method looks up Internet addresses in a BitTorrent injector swarm, as "
"explained [here](../concepts/how.md)."
msgstr ""
"کلائنٹ کنیکٹیویٹی اور انجیکٹر ایڈریسنگ کے بارے میں معلومات۔ پہلے سے طے شدہ "
"`bep5` طریقہ BitTorrent انجیکٹر بھیڑ میں انٹرنیٹ ایڈریس تلاش کرتا ہے، جیسا "
"کہ [یہاں] (../concepts/how.md) کی وضاحت کی گئی ہے۔"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"The public key used to verify signatures from injectors in the distributed "
"cache."
msgstr ""
"عوامی کلید تقسیم شدہ کیش میں انجیکٹر سے دستخطوں کی تصدیق کے لیے استعمال ہوتی "
"ہے۔"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Information on your local cache like the maximum content age, approximate "
"size of the cache, a button to purge it completely, and a link to the list "
"of announced cache entries."
msgstr ""
"آپ کے مقامی کیش پر معلومات جیسے مواد کی زیادہ سے زیادہ عمر، کیش کا تخمینی "
"سائز، اسے مکمل طور پر صاف کرنے کے لیے ایک بٹن، اور اعلان کردہ کیش اندراجات "
"کی فہرست کا لنک۔"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"The directory of the external, static cache if enabled (Ceno does not "
"currently use this)."
msgstr ""
"بیرونی، جامد کیش کی ڈائرکٹری اگر فعال ہو (Ceno فی الحال اسے استعمال نہیں "
"کرتا ہے)۔"

#: client/front-end.md:block 6 (unordered list)
#, fuzzy
#| msgid ""
#| "[Ceno Extension]: https://github.com/censorship-no/ceno-ext-settings/"
msgid "[Ceno Extension]: https://github.com/censorship-no/ceno-ext-settings/"
msgstr "[Ceno ایکسٹینشن]: https://github.com/censorship-no/ceno-ext-settings/"
