msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-05 20:50+0200\n"
"PO-Revision-Date: 2023-09-05 21:12+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Arabic <https://hosted.weblate.org/projects/censorship-no/"
"browser-bridging/ar/>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Weblate 5.0.1-dev\n"

#: browser/bridging.md:block 1 (header)
msgid "Helping other Ceno users browse the Web"
msgstr "مساعدة مستخدمي Ceno الآخرين على تصفح الويب"

#: browser/bridging.md:block 2 (paragraph)
msgid ""
"A peer-to-peer network is built from every node connected to it (yes, that "
"means you as well!). The more nodes, the stronger and more versatile the "
"network becomes. If you are running Ceno Browser from a country that does "
"not censor the Internet (or not as heavily as some), consider helping other "
"Ceno users by becoming a **bridge** node. You will then begin to route "
"traffic between clients living in heavily censored countries and Ceno "
"injectors. You will not be able to see their traffic (it will be sent "
"through an encrypted tunnel), nor will any of this traffic remain on your "
"device."
msgstr ""
"يتم بناء شبكة نظير إلى نظير من كل عقدة متصلة بها (نعم، هذا يعنيك أيضًا!). "
"كلما زاد عدد العقد، كلما أصبحت الشبكة أقوى وأكثر تنوعًا. إذا كنت تقوم بتشغيل "
"متصفح Ceno من بلد لا يفرض رقابة على الإنترنت (أو أقل من بعض الدول)، ففكر في "
"مساعدة مستخدمي Ceno الآخرين من خلال أن تصبح عقدة **جسر**. ستبدأ حينها في "
"توجيه حركة المرور بين العملاء الذين يعيشون في البلدان الخاضعة للرقابة "
"الشديدة وبين حاقنات Ceno. لن تكون قادرًا على رؤية حركة المرور الخاصة بهم "
"(سيتم إرسالها عبر نفق مشفر)، ولن تظل أي من حركة المرور هذه على جهازك."

#: browser/bridging.md:block 3 (quote)
msgid ""
"**Note:** The configuration described in this section may also help your "
"device to effectively seed content to others on the distributed cache, so "
"please consider applying it as well when using Ceno in a censoring country "
"(but keep in mind the [risks](../concepts/risks.md) of serving such content "
"to others)."
msgstr ""
"**ملاحظة:** قد تساعد التهيئة الموضحة في هذا القسم جهازك على توزيع المحتوى "
"بشكل فعال للآخرين على ذاكرة التخزين المؤقت الموزعة، لذا خذ بعين الإعتبار "
"إمكانية تطبيقها أيضًا عند استخدام Ceno في دولة تخضع للرقابة (ولكن تذكر "
"[المخاطر](../concepts/risks.md) عند تقديم محتوى كهذا للآخرين)."

#: browser/bridging.md:block 4 (header)
msgid "How to become a Ceno bridge"
msgstr "كيف تصبح جسرًا لـ Ceno"

#: browser/bridging.md:block 5 (paragraph)
msgid ""
"This functionality is already built into Ceno Browser. Your device will need "
"to be connected to a Wi-Fi network that has either UPnP enabled or explicit "
"port forwarding configured for Ceno. See the next sections for further "
"details."
msgstr ""
"هذه الخاصية مضمنة بالفعل في متصفح Ceno. سيحتاج جهازك إلى الاتصال بشبكة Wi-Fi "
"ثم تمكين UPnP أو إعادة توجيه منفذ صريح تم تهيئته لـ Ceno. انظر الأقسام "
"التالية لمزيد من التفاصيل."

#: browser/bridging.md:block 6 (paragraph)
msgid ""
"However, please note that Android will only allow a mobile device to act as "
"a proper bridge while you are actively using it, as power-saving features "
"will throttle the operation of Ceno otherwise."
msgstr ""
"ومع ذلك، يرجى ملاحظة أن أندرويد لا يسمح للجهاز المحمول بالعمل كجسر إلا أثناء "
"استخدامه فعليا، بما أن ميزات توفير الطاقة تؤدي إلى إعاقة تشغيل Ceno."

#: browser/bridging.md:block 7 (quote)
msgid ""
"**Technical note:** This is mainly due to Android's [Doze mode](https://"
"developer.android.com/training/monitoring-device-state/doze-standby "
"\"Android Developers – Optimize for Doze and App Standby\") slowing down the "
"operation of the native Ouinet library. Unfortunately, disabling battery "
"optimization for Ceno does not seem to exclude Ouinet from it. Your "
"particular device may also include its own power-saving features which may "
"interfere with Ceno; please check [Don't kill my app!][Don't kill my app!] "
"for your device's brand."
msgstr ""
"**ملاحظة تقنية:** يرجع هذا أساسًا إلى [وضع الغفوة](https://developer.android."
"com/training/monitoring-device-state/doze-standby \"Android Developers – "
"Optimize for Doze and App Standby\") في أندرويد والذي يبطئ تشغيل مكتبة "
"Ouinet الأصلية. لسوء الحظ، لا يبدو أن تعطيل توفير البطارية لـ Ceno لا يستبعد "
"Ouinet من ذلك. قد يشتمل جهازك الخاص أيضًا على ميزات توفير الطاقة الخاصة به "
"والتي قد تتداخل مع Ceno؛ يرجى التحقق من [Don't kill my app!][Don't kill my "
"app!] للعلامة التجارية لجهازك."

#: browser/bridging.md:block 8 (paragraph)
msgid ""
"Thus if you intend to have Ceno acting as a permanent, always-reachable "
"bridge, besides a properly configured Wi-Fi network you will need to:"
msgstr ""
"وبالتالي، إذا كنت تنوي جعل Ceno يعمل كجسر دائم يمكن الوصول إليه دائمًا، إلى "
"جانب شبكة Wi-Fi مهيأة بشكل صحيح، فستحتاج إلى:"

#: browser/bridging.md:block 9 (ordered list)
msgid "Have your device plugged in to power at all times."
msgstr "أترك جهازك يُشحن طوال الوقت."

#: browser/bridging.md:block 9 (ordered list)
msgid "Have the device's screen on at all times."
msgstr "أترك شاشة الجهاز مُشغلة طوال الوقت."

#: browser/bridging.md:block 9 (ordered list)
msgid ""
"One convenient way of doing this without much power consumption and "
"obnoxious, permanent lighting is using Android's screen saver: enable it "
"under *Settings / Display / Screen saver* (or *Daydream* in some versions), "
"pick the *Clock* widget, choose *When to start screen saver* in the menu and "
"select *While charging* or *Either*. A very dimmed down clock will appear on "
"a black background while the device is not active."
msgstr ""
"إحدى الطرق الملائمة للقيام بذلك دون استهلاك الكثير من الطاقة والإضاءة "
"الدائمة المزعجة هي استخدام شاشة التوقف في أندرويد: قم بتمكينها ضمن "
"*الإعدادات / العرض / شاشة التوقف* (أو *أحلام اليقظة* في بعض الإصدارات)، اختر "
"أداة *الساعة*، اختر *متى تبدأ شاشة التوقف* في القائمة وحدد *أثناء الشحن* أو "
"* إما هذا و إما ذاك*. ستظهر ساعة خافتة للغاية على خلفية سوداء عندما يكون "
"الجهاز غير نشط."

#: browser/bridging.md:block 9 (ordered list)
msgid ""
"Please note that you should not use the power button to lock the device as "
"this will turn the screen off. Instead, just wait for the device to lock "
"itself with the screen on."
msgstr ""
"يرجى ملاحظة أنه لا يجب استخدام زر الطاقة لقفل الجهاز لأن هذا سيؤدي إلى إيقاف "
"تشغيل الشاشة. بدلاً من ذلك، فقط انتظر حتى يقفل الجهاز نفسه مع ترك الشاشة "
"مشغلة."

#: browser/bridging.md:block 10 (paragraph)
msgid ""
"If that setup is not an option for you, do not desist yet! If you have a "
"computer with good connectivity that stays on most of the time, please "
"continue reading."
msgstr ""
"إذا لم يكن هذا الإعداد متاحًا لك، فلا تستسلم بعد! إذا كان لديك جهاز كمبيوتر "
"يتمتع باتصال جيد ويظل مشغلا معظم الوقت، فيرجى متابعة القراءة."

#: browser/bridging.md:block 11 (header)
msgid "Running a bridge on a computer"
msgstr "تشغيل جسر على جهاز كمبيوتر"

#: browser/bridging.md:block 12 (paragraph)
msgid ""
"If your computer supports [Docker containers][docker], you can run a pre-"
"configured Ceno client on it to act as a bridge. If Docker is not yet "
"installed, please follow the instructions to [install the Docker Engine]"
"[docker-install] in your platform. For Debian derivatives like Ubuntu or "
"Linux Mint, you can just run: `sudo apt install docker.io`"
msgstr ""
"إذا كان جهاز الكمبيوتر الخاص بك يدعم [حاويات دوكر][docker]، فيمكنك تشغيل "
"عميل Ceno عليه تم تهيئته مسبقًا ليكون بمثابة جسر. إذا لم يتم تثبيت دوكر بعد، "
"فالرجاء اتباع التعليمات لـ [تثبيت محرك دوكر][docker-install] في منصتك. "
"بالنسبة لمشتقات دبيان مثل أوبونتو أو لينكس مينت، يمكنك فقط تشغيل: `sudo apt "
"install docker.io`"

#: browser/bridging.md:block 13 (paragraph)
msgid ""
"To deploy a Ceno client container you only need to run the following command "
"on a terminal (it looks scary but you can just copy and paste it as is on "
"the command line):"
msgstr ""
"لنشر حاوية عميل Ceno، ما عليك سوى تشغيل الأمر التالي على محطة (يبدو الأمر "
"مخيفًا ولكن يمكنك نسخه ولصقه كما هو في سطر الأوامر):"

#: browser/bridging.md:block 15 (paragraph)
msgid ""
"If your computer is not based on GNU/Linux, the command needs to be slightly "
"different:"
msgstr ""
"إذا كان جهاز الكمبيوتر الخاص بك لا يعتمد على جنو/ لينكس، فيجب أن يكون الأمر "
"مختلفًا قليلاً:"

#: browser/bridging.md:block 17 (paragraph)
msgid ""
"The command will start a container named `ceno-client` that will run on "
"every boot unless you explicitly tell it to stop. Please check the [Ceno "
"Docker client documentation][ceno-client-doc] for more information on how to "
"manipulate the container."
msgstr ""
"سيبدأ الأمر حاوية تسمى `ceno-client` والتي سيتم تشغيلها عند كل بدء تشغيل ما "
"لم تطلب منها التوقف. يرجى مراجعة [وثائق دوكر عميل Ceno][ceno-client-doc] "
"للحصول على مزيد من المعلومات حول كيفية التعامل مع الحاوية."

#: browser/bridging.md:block 18 (quote)
msgid ""
"**Note:** This client has no *Ceno Settings*: when instructed below to "
"access that page, open instead the [client front-end](../client/front-end."
"md), which contains mostly equivalent information."
msgstr ""
"**ملاحظة:** هذا العميل ليس لديه *إعدادات Ceno*: عندما يُطلب منك أدناه الوصول "
"إلى تلك الصفحة، افتح بدلاً من ذلك [واجهة المستخدم](../client/front-end.md)، "
"والتي تحتوي في الغالب على معلومات مماثلة."

#: browser/bridging.md:block 19 (header)
msgid "Enabling UPnP on your Wi-Fi router"
msgstr "تمكين UPnP على جهاز توجيه Wi-Fi الخاص بك"

#: browser/bridging.md:block 20 (paragraph)
msgid ""
"[UPnP][UPnP] is the easiest way of making your Ceno Browser (or computer "
"client) reachable to the Ceno network. The [Ceno Settings](settings.md) page "
"will indicate the UPnP status on your local network."
msgstr ""
"[UPnP][UPnP] هي أسهل طريقة لجعل متصفح Ceno (أو عميل الكمبيوتر) قابلاً للوصول "
"عبر شبكة Ceno. ستشير صفحة [إعدادات Ceno](settings.md) إلى حالة UPnP على "
"شبكتك المحلية."

#: browser/bridging.md:block 21 (quote)
msgid ""
"**Note:** Enabling UPnP on the Wi-Fi router may expose devices on your "
"network to external interference. Please make yourself [aware of the risks]"
"[upnp-risks] and also consider using alternative methods as explained below."
msgstr ""
"**ملاحظة:** قد يؤدي تمكين UPnP على موجه Wi-Fi إلى تعريض الأجهزة الموجودة على "
"شبكتك لتداخلات خارجية. يرجى أن تكون على [دراية بالمخاطر][upnp-risks] و خذ "
"بعين الإعتبار استخدام طرق بديلة كما هو موضح أدناه."

#: browser/bridging.md:block 22 (paragraph)
msgid ""
"A status like the one below indicates that UPnP is not available or not "
"working on your WiFi router:"
msgstr ""
"تشير الحالة أدناه إلى أن UPnP غير متوفر أو لا يعمل على جهاز توجيه WiFi الخاص "
"بك:"

#: browser/bridging.md:block 23 (quote)
msgid "**Reachability status**"
msgstr "**حالة إمكانية الوصول**"

#: browser/bridging.md:block 23 (quote)
msgid "**UPnP status**"
msgstr "**حالة UPnP**"

#: browser/bridging.md:block 24 (paragraph)
msgid ""
"The status below indicates that UPnP is likely working and Ceno is currently "
"verifying connectivity:"
msgstr ""
"تشير الحالة أدناه إلى أن UPnP يعمل على الأرجح وأن Ceno يتحقق حاليًا من "
"الاتصال:"

#: browser/bridging.md:block 26 (paragraph)
msgid ""
"The status below indicates that UPnP is working and you can bridge "
"connections for other Ceno users:"
msgstr ""
"تشير الحالة أدناه إلى أن UPnP يعمل ويمكنك ربط الاتصال بمستخدمي Ceno الآخرين:"

#: browser/bridging.md:block 28 (quote)
msgid ""
"**Note:** Even if UPnP is working, your router may still not be reachable "
"from the outside. This can be the case when *Ceno Settings* reports "
"*External UDP endpoints* which look like [CGNAT][CGNAT] addresses `100.X.Y.Z:"
"N` with X between 64 and 127 (increasingly common among home ISPs), or like "
"private addresses `10.X.Y.Z:N`, `172.X.Y.Z:N` with X between 16 and 31, and "
"`192.168.X.Y:N`. If so, please contact your ISP or network administrator to "
"get a public address on your router or to establish port forwardings to the "
"external endpoint."
msgstr ""
"**ملاحظة:** حتى إذا كان UPnP يعمل، فقد يتعذر الوصول إلى جهاز التوجيه الخاص "
"بك من الخارج. يمكن أن يحدث هذا عندما تكون *إعدادات Ceno* تشير إلى *نقاط "
"نهاية UDP الخارجية* التي تبدو مثل عناوين [CGNAT][CGNAT] `100.X.Y.Z:N` حيث X "
"بين 64 و 127 (شائع جدًا عند مزودي خدمة الإنترنت المنزلية)، أو مثل العناوين "
"الخاصة `10.X.Y.Z:N` و `172.X.Y.Z:N` حيث X بين 16 و 31، و `192.168.X.Y:N`. "
"إذا كان الأمر كذلك، فيرجى الاتصال بمزود خدمة الإنترنت أو مسؤول الشبكة للحصول "
"على عنوان عام على جهاز التوجيه الخاص بك أو لتأسيس عمليات إعادة توجيه المنفذ "
"إلى نقاط النهاية الخارجية."

#: browser/bridging.md:block 29 (paragraph)
msgid ""
"There are many Wi-Fi routers on the market and each has their own particular "
"features. Herein a list of some manufacturers' instructions for enabling "
"UPnP:"
msgstr ""
"هناك العديد من أجهزة توجيه Wi-Fi في السوق ولكل منها ميزاته الخاصة. فيما يلي "
"قائمة ببعض إرشادات الشركات المصنعة لتمكين UPnP:"

#: browser/bridging.md:block 30 (unordered list)
msgid "[Linksys](https://www.linksys.com/us/support-article?articleNum=138290)"
msgstr ""
"[Linksys](https://www.linksys.com/ae/support-article?articleNum=138290)"

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[D-Link](https://eu.dlink.com/uk/en/support/faq/routers/wired-routers/di-"
"series/how-do-i-enable-upnp-on-my-router)"
msgstr ""
"[D-Link](https://eu.dlink.com/uk/en/support/faq/routers/wired-routers/di-"
"series/how-do-i-enable-upnp-on-my-router)"

#: browser/bridging.md:block 30 (unordered list)
msgid "[Huawei](https://consumer.huawei.com/ph/support/content/en-us00275342/)"
msgstr ""
"[Huawei](https://consumer.huawei.com/ae/support/content/ar-eg15806294/)"

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[Xfinity](https://www.xfinity.com/support/articles/configure-device-"
"discovery-for-wifi)"
msgstr ""
"[Xfinity](https://www.xfinity.com/support/articles/configure-device-"
"discovery-for-wifi)"

#: browser/bridging.md:block 30 (unordered list)
msgid "[TP-Link](https://community.tp-link.com/us/home/kb/detail/348)"
msgstr "[TP-Link](https://community.tp-link.com/us/home/kb/detail/348)"

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[Netgear](https://kb.netgear.com/24306/How-do-I-enable-Universal-Plug-and-"
"Play-on-my-Nighthawk-router)"
msgstr ""
"[Netgear](https://kb.netgear.com/24306/How-do-I-enable-Universal-Plug-and-"
"Play-on-my-Nighthawk-router)"

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[Ubiquiti](https://www.geekzone.co.nz/forums.asp?"
"forumid=66&topicid=205740&page_no=5#1725168)"
msgstr ""
"[Ubiquiti](https://www.geekzone.co.nz/forums.asp?"
"forumid=66&topicid=205740&page_no=5#1725168)"

#: browser/bridging.md:block 31 (header)
msgid "Using port forwarding as an alternative to UPnP"
msgstr "استخدام إعادة توجيه المنفذ كبديل لـ UPnP"

#: browser/bridging.md:block 32 (paragraph)
msgid ""
"Instead of enabling UPnP on your router, you can create a port forwarding "
"rule to make sure that connections from the Ceno network are forwarded to "
"your device. You will need to login to the router's administrative interface "
"and locate the *port forwarding* option. To see which IP address you need to "
"forward the connections to and the relevant port, open the *Ceno Settings* "
"page and look under the *Local UDP endpoints*."
msgstr ""
"بدلاً من تمكين UPnP على جهاز التوجيه الخاص بك، يمكنك إنشاء قاعدة إعادة توجيه "
"المنفذ للتأكد من إعادة توجيه الاتصالات من شبكة Ceno إلى جهازك. ستحتاج إلى "
"تسجيل الدخول إلى الواجهة الإدارية لجهاز التوجيه وتحديد خيار *إعادة توجيه "
"المنفذ*. لمعرفة عنوان IP الذي تحتاجه لإعادة توجيه الاتصالات والمنفذ الصحيح، "
"افتح صفحة *إعدادات Ceno* وانظر *نقاط نهاية UDP المحلية*."

#: browser/bridging.md:block 33 (quote)
msgid "**Local UDP endpoints**"
msgstr "**نقاط نهاية UDP المحلية**"

#: browser/bridging.md:block 34 (paragraph)
msgid ""
"The port forwarding must be for the UDP protocol (not TCP). Ceno chooses a "
"random port on first run and keeps it for subsequent runs, but your device's "
"local network IP address may change from time to time. Thus you should "
"periodically review the *Ceno Settings* page to see that your device is "
"reachable to the Ceno network."
msgstr ""
"يجب أن تكون إعادة توجيه المنفذ لبروتوكول UDP (وليس TCP). يختار Ceno منفذًا "
"عشوائيًا عند التشغيل الأول ويحتفظ به لعمليات التشغيل اللاحقة، ولكن قد يتغير "
"عنوان IP للشبكة المحلية لجهازك من وقت لآخر. وبالتالي، يجب عليك مراجعة صفحة "
"*إعدادات Ceno* بشكل دوري لمعرفة هل جهازك يمكن الوصول إليه من خلال شبكة Ceno."

#: browser/bridging.md:block 35 (quote)
msgid ""
"**Technical note:** Alternatively, you can make sure that the router always "
"assigns the same IP address to your device (e.g. via a static DHCP lease for "
"the device's MAC address)."
msgstr ""
"**ملاحظة تقنية:** بدلاً من ذلك، يمكنك التأكد من أن جهاز التوجيه يعيّن دائمًا "
"عنوان IP نفسه لجهازك (على سبيل المثال، عبر عقد إيجار DHCP ثابت لعنوان MAC "
"الخاص بالجهاز)."

#: browser/bridging.md:block 35 (quote)
msgid ""
"[Doze mode]: https://developer.android.com/training/monitoring-device-state/"
"doze-standby"
msgstr ""
"[وضع الغفوة]: https://developer.android.com/training/monitoring-device-state/"
"doze-standby"

#: browser/bridging.md:block 35 (quote)
msgid "[Don't kill my app!]: https://dontkillmyapp.com/"
msgstr "[Don't kill my app!] \"(لا تقتل تطبيقي!): https://dontkillmyapp.com/"

#: browser/bridging.md:block 35 (quote)
msgid "[docker]: https://en.wikipedia.org/wiki/Docker_(software)"
msgstr ""
"[docker]: https://ar.wikipedia.org/wiki/"
"%D8%AF%D9%88%D9%83%D8%B1_(%D8%A8%D8%B1%D9%85%D8%AC%D9%8A%D8%A9)"

#: browser/bridging.md:block 35 (quote)
msgid "[docker-install]: https://docs.docker.com/engine/install/"
msgstr "[docker-install]: https://docs.docker.com/engine/install/"

#: browser/bridging.md:block 35 (quote)
msgid ""
"[ceno-client-doc]: https://github.com/censorship-no/ceno-docker-"
"client#running-the-client"
msgstr ""
"[ceno-client-doc]: https://github.com/censorship-no/ceno-docker-client"
"#running-the-client"

#: browser/bridging.md:block 35 (quote)
msgid "[UPnP]: https://en.wikipedia.org/wiki/Universal_Plug_and_Play"
msgstr ""
"[UPnP]: https://ar.wikipedia.org/wiki/"
"%D8%A7%D9%84%D8%AA%D9%88%D8%B5%D9%8A%D9%84_%D9%88%D8%A7%D9%84%D8%AA%D8%B4%D8%BA%D9%8A%D9%84_%D8%A7%D9%84%D8%B9%D8%A7%D9%84%D9%85%D9%8A"

#: browser/bridging.md:block 35 (quote)
msgid ""
"[upnp-risks]: https://www.howtogeek.com/122487/htg-explains-is-upnp-a-"
"security-risk"
msgstr ""
"[upnp-risks]: https://www.howtogeek.com/122487/htg-explains-is-upnp-a-"
"security-risk"

#: browser/bridging.md:block 35 (quote)
msgid "[CGNAT]: https://en.wikipedia.org/wiki/Carrier-grade_NAT"
msgstr "[CGNAT]: https://en.wikipedia.org/wiki/Carrier-grade_NAT"
