msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-03 02:50+0200\n"
"PO-Revision-Date: 2023-09-01 23:03+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Arabic <https://hosted.weblate.org/projects/censorship-no/qsg/"
"ar/>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Weblate 5.0.1-dev\n"

#
#: intro/qsg.md:block 1 (header)
msgid "Quick start guide"
msgstr "دليل البدء السريع"

#: intro/qsg.md:block 2 (paragraph)
msgid ""
"Ceno Browser allows you to access any website on the Internet, even if it is "
"censored in your country. Ceno uses a peer-to-peer infrastructure to route "
"your requests, as well as to store and share retrieved content with others. "
"[Read more about Ceno](ceno.md)."
msgstr ""
"يسمح لك متصفح Ceno بالوصول إلى أي موقع ويب على الإنترنت، حتى لو كان خاضعا "
"للرقابة في بلدك. يستخدم Ceno بنية تحتية من نظير إلى نظير لتوجيه طلباتك، "
"بالإضافة إلى تخزين ومشاركة المحتوى الذي تم استرداده مع الآخرين. [اقرأ المزيد "
"عن Ceno](ceno.md)."

#: intro/qsg.md:block 3 (header)
msgid "How to get started"
msgstr "كيفية البدء"

#: intro/qsg.md:block 4 (paragraph)
msgid "You will need an Android device:"
msgstr "ستحتاج إلى جهاز أندرويد (Android):"

#: intro/qsg.md:block 5 (ordered list)
msgid ""
"Install Ceno Browser from [Google Play][ceno-gplay], [GitHub][ceno-gh] or "
"[Paskoocheh][ceno-pask]. *No special permissions are needed*."
msgstr ""
"قم بتثبيت متصفح Ceno من [Google Play][ceno-gplay] أو [GitHub][ceno-gh] أو "
"[Paskoocheh][ceno-pask]. *ليست هناك حاجة إلى أذونات خاصة*."

#: intro/qsg.md:block 5 (ordered list)
msgid "Run it."
msgstr "قم بتشغيله."

#: intro/qsg.md:block 5 (ordered list)
msgid ""
"Browse pages normally to help other users access them; if concerned about "
"privacy for some page or if it is not loading as expected, use a private tab "
"(see [public vs. private browsing](../concepts/public-private.md))."
msgstr ""
"تصفح الصفحات بشكل طبيعي لمساعدة المستخدمين الآخرين على الوصول إليها؛ إذا كنت "
"قلقا بشأن خصوصية صفحة ما أو إذا لم يتم تحميلها كما هو متوقع، فاستخدم علامة "
"تبويب خاصة (راجع [التصفح العام مقابل التصفح الخاص](../concepts/public-"
"private.md))."

#: intro/qsg.md:block 5 (ordered list)
msgid "Tap on the Ceno notification to stop it completely."
msgstr "اضغط على إشعار Ceno لإيقافه تماما."

#: intro/qsg.md:block 6 (paragraph)
msgid "Detailed installation instructions are [here](../browser/install.md)."
msgstr "تعليمات التثبيت المفصلة [هنا](../browser/install.md)."

#: intro/qsg.md:block 7 (header)
msgid "Configuration"
msgstr "تهيئة"

#: intro/qsg.md:block 8 (paragraph)
msgid ""
"Ceno Browser should work out-of-the-box. You can find some [diagnostics and "
"settings](../browser/settings.md) under the *Ceno* menu entry."
msgstr ""
"يجب أن يعمل متصفح Ceno مباشرة. يمكنك العثور على بعض [التشخيصات والإعدادات]"
"(../browser/settings.md) ضمن قائمة الدخول *Ceno*."

#: intro/qsg.md:block 9 (paragraph)
msgid ""
"If you want to make sure that your app is also helping others access blocked "
"content, please [read this section](../browser/bridging.md)."
msgstr ""
"إذا كنت تريد التأكد من أن تطبيقك يساعد الآخرين أيضا في الوصول إلى المحتوى "
"المحظور، فيرجى [قراءة هذا القسم](../browser/bridging.md)."

#: intro/qsg.md:block 10 (header)
msgid "More questions?"
msgstr "المزيد من الأسئلة؟"

#: intro/qsg.md:block 11 (unordered list)
msgid "Please see the [FAQ](faq.md)."
msgstr "يرجى الاطلاع على [الأسئلة الشائعة](faq.md)."

#: intro/qsg.md:block 11 (unordered list)
msgid "Refer to the [troubleshooting guide](../browser/troubleshooting.md)."
msgstr ""
"ارجع إلى [دليل استكشاف الأخطاء وإصلاحها](../browser/troubleshooting.md)."

#: intro/qsg.md:block 11 (unordered list)
#, fuzzy
#| msgid ""
#| "Contact us by writing to [cenoers@equalit.ie](mailto:cenoers@equalit.ie)."
msgid ""
"Contact us by writing to [cenoers@equalitie.org](mailto:cenoers@equalitie."
"org)."
msgstr ""
"اتصل بنا عن طريق الكتابة إلى [cenoers@equalitie.org](mailto:"
"cenoers@equalitie.org)."

#: intro/qsg.md:block 11 (unordered list)
#, fuzzy
#| msgid ""
#| "[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit."
#| "ceno"
msgid ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno"
msgstr ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno"

#: intro/qsg.md:block 11 (unordered list)
#, fuzzy
#| msgid "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"
msgid "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"
msgstr "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"

#: intro/qsg.md:block 11 (unordered list)
msgid "[ceno-pask]: https://paskoocheh.com/tools/124/android.html"
msgstr "[ceno-pask]: https://paskoocheh.com/tools/124/android.html"
