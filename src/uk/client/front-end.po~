#
msgid ""
msgstr ""
"PO-Revision-Date: 2023-05-17 03:10+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Ukrainian <https://hosted.weblate.org/projects/censorship-no/"
"client-front-end/uk/>\n"
"Language: uk\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.18-dev\n"

#: client/front-end.md:block 1 (header)
msgid "Annex: The Ouinet client front-end"
msgstr "Додаток: Зовнішній інтерфейс клієнта Ouinet"

#: client/front-end.md:block 2 (paragraph)
msgid ""
"The Ouinet client (as run by e.g. Ceno Browser) offers a front-end page with "
"some information and actions which may be useful for debugging the client. "
"Many of them are also offered by the Ceno Extension via the [*Ceno Settings* "
"page](../browser/settings.md), though others are only available here."
msgstr ""
"У клієнта Ouinet (який запускається, наприклад, браузером Ceno) є службовий "
"інтерфейс із довідковою інформацією та командами, що можуть бути корисними "
"для налагодження клієнта. Багато з цих команд також пропонує розширення Ceno "
"через сторінку [*Налаштування Ceno*](../browser/settings.md), проте деякі "
"команди доступні лише тут."

#: client/front-end.md:block 3 (paragraph)
msgid ""
"The front-end is accessible using any plain Web browser running on the same "
"device (you can use Ceno too). Its default address is <http://127.0.0.1:8078/"
">. If you open it, you will see something like the figure below."
msgstr ""
"Інтерфейс доступний через будь-який веб-браузер, запущений на тому ж "
"пристрої (Ви можете використовувати Ceno також). Типова адреса інтерфейсу – "
"<http://127.0.0.1:8078/>. Під час його відкривання Ви побачите щось схоже на "
"малюнок нижче."

#: client/front-end.md:block 4 (paragraph)
msgid "![Figure: The client front-end](images/front-end.png)"
msgstr "![Малюнок: Зовнішній інтерфейс клієнта](images/front-end.png)"

#: client/front-end.md:block 5 (paragraph)
msgid "The items shown in the page include:"
msgstr "На сторінці відображаються такі елементи:"

#: client/front-end.md:block 6 (unordered list)
msgid ""
"A link to enable the client as a certificate authority (CA) at your browser, "
"since the client needs to intercept HTTPS traffic."
msgstr ""
"Посилання для налаштування Ouinet як центру сертифікації (ЦС) у Вашому "
"браузері, оскільки клієнту необхідно перехоплювати HTTPS-трафік."

#: client/front-end.md:block 6 (unordered list)
msgid ""
"You only need this to use a plain browser for testing the Ouinet client, in "
"which case you will also have to configure its HTTP/HTTPS proxies to "
"`127.0.0.1:8077`, and manually enable the [Ceno Extension][Ceno Extension] "
"for injection to work. We very strongly recommend using a *separate, "
"specific browser profile* for this purpose."
msgstr ""
"Вам це потрібно лише скориставшись звичайним браузером для тестування "
"клієнта Ouinet, у цьому випадку Вам також доведеться налаштувати його HTTP/"
"HTTPS-проксі на `127.0.0.1:8077` та вручну запустити [Розширення Ceno]"
"[Розширення Ceno], щоб інʼєкція працювала. Ми радимо використовувати "
"*окремий профіль браузера* для цієї мети."

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Please note that none of this needs to be done for Ceno Browser, since it is "
"already configured like that."
msgstr ""
"Зверніть увагу, жодна з цих дій не стосується браузера Ceno, оскільки він "
"вже налаштований відповідно."

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Buttons to enable or disable the different mechanisms used by the client to "
"access content."
msgstr ""
"Кнопки для ввімкнення або вимкнення різних механізмів, які клієнт "
"використовує для доступу до вмісту."

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Selectors to choose different log levels, like the default `INFO` "
"(informational messages, warnings and errors) or `DEBUG` (verbose output "
"useful for reporting errors). The log file can also be enabled and retrieved "
"from here."
msgstr ""
"Селектори для вибору різних рівнів журналу, наприклад, за замовчуванням "
"`INFO` (інформаційні повідомлення, попередження та помилки) або `DEBUG` "
"(докладний звіт з повідомленням про помилки). Звідси також можна ввімкнути "
"та отримати файл журналу ."

#: client/front-end.md:block 6 (unordered list)
msgid ""
"When enabling the log file, the log level is automatically set to `DEBUG` "
"(though you may change it again from here). When disabling the log file, the "
"original log level is restored."
msgstr ""
"При активуванні файлу журналу, рівень журналу автоматично встановлюється на "
"значенні `DEBUG` (хоча Ви можете змінити його). У разі вимкнення файлу "
"журналу, буде відновлено вихідний рівень."

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Global client state and version information. Useful when reporting errors."
msgstr ""
"Загальна інформація про стан та версію клієнта. Корисно під час створення "
"звітів про помилки."

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Information about client connectivity and injector addressing. The default "
"`bep5` method looks up Internet addresses in a BitTorrent injector swarm, as "
"explained [here](../concepts/how.md)."
msgstr ""
"Інформація про підключення клієнтів та адресацію інʼєкторів. За "
"замовчуванням, метод`bep5` шукає Інтернет-адреси серед «рою» інʼєкторів "
"BitTorrent, як описано в [цьому розділі](../concepts/how.md)."

#: client/front-end.md:block 6 (unordered list)
msgid ""
"The public key used to verify signatures from injectors in the distributed "
"cache."
msgstr ""
"Відкритий ключ, що використовується для перевірки підписів від інʼєкторів у "
"розподіленому кеші."

#: client/front-end.md:block 6 (unordered list)
msgid ""
"Information on your local cache like the maximum content age, approximate "
"size of the cache, a button to purge it completely, and a link to the list "
"of announced cache entries."
msgstr ""
"Інформація про Ваш локальний кеш, як-от: максимальний вік вмісту, приблизний "
"розмір кешу, посилання на список анонсованих записів кешу, а також кнопка "
"для повного очищення кешу."

#: client/front-end.md:block 6 (unordered list)
msgid ""
"The directory of the external, static cache if enabled (Ceno does not "
"currently use this)."
msgstr ""
"Каталог зовнішнього статичного кешу, якщо він увімкнений (нині Ceno його не "
"використовує)."

#: client/front-end.md:block 6 (unordered list)
#, fuzzy
#| msgid ""
#| "[Ceno Extension]: https://github.com/censorship-no/ceno-ext-settings/"
msgid "[Ceno Extension]: https://github.com/censorship-no/ceno-ext-settings/"
msgstr "[Розширення Ceno]: https://github.com/censorship-no/ceno-ext-settings/"
