#
msgid ""
msgstr ""
"PO-Revision-Date: 2023-09-05 22:01+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Ukrainian <https://hosted.weblate.org/projects/censorship-no/"
"qsg/uk/>\n"
"Language: uk\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 5.0.1-dev\n"

#: intro/qsg.md:block 1 (header)
msgid "Quick start guide"
msgstr "Короткий посібник"

#: intro/qsg.md:block 2 (paragraph)
msgid ""
"Ceno Browser allows you to access any website on the Internet, even if it is "
"censored in your country. Ceno uses a peer-to-peer infrastructure to route "
"your requests, as well as to store and share retrieved content with others. "
"[Read more about Ceno](ceno.md)."
msgstr ""
"Браузер CENO дозволяє отримати доступ до будь-якого сайту в Інтернеті, "
"навіть якщо він цензурується Вашою країною. CENO використовує однорангову "
"(peer-to-peer) інфраструктуру для маршрутизації Ваших запитів, а також для "
"зберігання та розповсюдження отриманого вмісту іншим користувачам мережі. "
"[Докладніше про CENO](ceno.md)."

#: intro/qsg.md:block 3 (header)
msgid "How to get started"
msgstr "З чого почати"

#: intro/qsg.md:block 4 (paragraph)
msgid "You will need an Android device:"
msgstr "Вам знадобиться пристрій Android:"

#: intro/qsg.md:block 5 (ordered list)
msgid ""
"Install Ceno Browser from [Google Play][ceno-gplay], [GitHub][ceno-gh] or "
"[Paskoocheh][ceno-pask]. *No special permissions are needed*."
msgstr ""
"Встановіть браузер CENO з [Google Play][ceno-gplay], [GitHub][ceno-gh] або "
"[Paskoocheh][ceno-pask]. *Жодних особливих дозволів не потрібно*."

#: intro/qsg.md:block 5 (ordered list)
msgid "Run it."
msgstr "Запустіть його."

#: intro/qsg.md:block 5 (ordered list)
msgid ""
"Browse pages normally to help other users access them; if concerned about "
"privacy for some page or if it is not loading as expected, use a private tab "
"(see [public vs. private browsing](../concepts/public-private.md))."
msgstr ""
"Переглядайте сторінки як зазвичай, щоб допомогти іншим користувачам отримати "
"до них доступ; якщо Вас непокоїть конфіденційність якоїсь сторінки або якщо "
"вона завантажується не так, як очікувалося – використовуйте приватну вкладку "
"(дивіться [публічний або приватний перегляд](../concepts/public-private.md))."

#: intro/qsg.md:block 5 (ordered list)
msgid "Tap on the Ceno notification to stop it completely."
msgstr "Натисніть на сповіщення CENO, щоб повністю зупинити його."

#: intro/qsg.md:block 6 (paragraph)
msgid "Detailed installation instructions are [here](../browser/install.md)."
msgstr ""
"Детальні інструкції зі встановлення розташовані [тут](../browser/install.md)."

#: intro/qsg.md:block 7 (header)
msgid "Configuration"
msgstr "Налаштування"

#: intro/qsg.md:block 8 (paragraph)
msgid ""
"Ceno Browser should work out-of-the-box. You can find some [diagnostics and "
"settings](../browser/settings.md) under the *Ceno* menu entry."
msgstr ""
"Браузер CENO повинен працювати «з коробки». Ви знайдете [діагностику та "
"налаштування](../browser/settings.md) під пунктом меню *CENO*."

#: intro/qsg.md:block 9 (paragraph)
msgid ""
"If you want to make sure that your app is also helping others access blocked "
"content, please [read this section](../browser/bridging.md)."
msgstr ""
"Якщо Ви хочете переконатися, що Ваша програма також допомагає іншим отримати "
"доступ до заблокованого вмісту, будь ласка [прочитайте цей розділ](../"
"browser/bridging.md)."

#: intro/qsg.md:block 10 (header)
msgid "More questions?"
msgstr "Залишилися питання?"

#: intro/qsg.md:block 11 (unordered list)
msgid "Please see the [FAQ](faq.md)."
msgstr "Будь ласка, перегляньте [ЧаП](faq.md)."

#: intro/qsg.md:block 11 (unordered list)
msgid "Refer to the [troubleshooting guide](../browser/troubleshooting.md)."
msgstr ""
"Зверніться до [посібника з усунення несправностей](../browser/"
"troubleshooting.md)."

#: intro/qsg.md:block 11 (unordered list)
msgid ""
"Contact us by writing to [cenoers@equalitie.org](mailto:cenoers@equalitie."
"org)."
msgstr ""
"Звʼяжіться з нами, написавши на адресу [cenoers@equalitie.org](mailto:"
"cenoers@equalitie.org)."

#: intro/qsg.md:block 11 (unordered list)
msgid ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.Ceno"
msgstr ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno"

#: intro/qsg.md:block 11 (unordered list)
msgid "[ceno-gh]: https://github.com/censorship-no/Ceno-browser/releases"
msgstr "[ceno-gh]: https://gitlab.com/censorship-no/ceno-browser/releases"

#: intro/qsg.md:block 11 (unordered list)
msgid "[ceno-pask]: https://paskoocheh.com/tools/124/android.html"
msgstr "[ceno-pask]: https://paskoocheh.com/tools/124/android.html"

#: intro/qsg.md:block 11 (unordered list)
msgid ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno"
msgstr ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno"

#: intro/qsg.md:block 11 (unordered list)
msgid "[ceno-gh]: https://gitlab.com/censorship-no/ceno-browser/releases"
msgstr "[ceno-gh]: https://gitlab.com/censorship-no/ceno-browser/releases"

#, fuzzy
#~| msgid "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"
#~ msgid "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"
#~ msgstr "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"
