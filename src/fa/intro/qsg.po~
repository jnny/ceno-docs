msgid ""
msgstr ""
"PO-Revision-Date: 2023-09-05 22:01+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Persian <https://hosted.weblate.org/projects/censorship-no/"
"qsg/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.0.1-dev\n"

#
#: intro/qsg.md:block 1 (header)
msgid "Quick start guide"
msgstr "راهنمای آغاز سریع"

#: intro/qsg.md:block 2 (paragraph)
msgid ""
"Ceno Browser allows you to access any website on the Internet, even if it is "
"censored in your country. Ceno uses a peer-to-peer infrastructure to route "
"your requests, as well as to store and share retrieved content with others. "
"[Read more about Ceno](ceno.md)."
msgstr ""
"مرورگر CENO به شما امکان دسترسی به هر وب‌سایتی روی اینترنت را می‌دهد، حتی اگر "
"آن وب‌سایت در کشورتان سانسور شده باشد. CENO از یک زیرساخت همتا-به-همتا "
"استفاده می‌کند تا درخواست‌های شما را رهیابی کند، و نیز محتوای بازیابی‌شده را "
"ذخیره نماید و با دیگران به اشتراک گذارد. [درباره‌ی سنو بیشتر بخوانید](ceno."
"md)."

#: intro/qsg.md:block 3 (header)
msgid "How to get started"
msgstr "چگونه آغاز کنید"

#: intro/qsg.md:block 4 (paragraph)
msgid "You will need an Android device:"
msgstr "شما به یک دستگاه اندروید نیاز خواهید داشت:"

#: intro/qsg.md:block 5 (ordered list)
msgid ""
"Install Ceno Browser from [Google Play][ceno-gplay], [GitHub][ceno-gh] or "
"[Paskoocheh][ceno-pask]. *No special permissions are needed*."
msgstr ""
"مرورگر CENO را از [گوگل پلی][ceno-gplay]، [گیت‌هاب][ceno-gh] یا [پس‌کوچه][ceno-"
"pask] دریافت و نصب کنید. *به هیچ جواز خاصی نیازی نیست*."

#: intro/qsg.md:block 5 (ordered list)
msgid "Run it."
msgstr "آن را اجرا کنید."

#: intro/qsg.md:block 5 (ordered list)
msgid ""
"Browse pages normally to help other users access them; if concerned about "
"privacy for some page or if it is not loading as expected, use a private tab "
"(see [public vs. private browsing](../concepts/public-private.md))."
msgstr ""
"مانند معمول صفحه‌ها را مرور کنید تا به کاربران دیگر نیز کمک کنید به آن‌ها "
"دسترسی پیدا کنند؛ اگر در مورد صفحه‌ای دلواپس حریم خصوصی‌تان بودید یا اگر "
"صفحه‌ای مطابق انتظارتان بار نشد، از یک زبانه‌ی خصوصی استفاده کنید (بنگرید به "
"[مقایسه‌ی مرور عمومی و مرور خصوصی](concepts/public-private.md/..)."

#: intro/qsg.md:block 5 (ordered list)
msgid "Tap on the Ceno notification to stop it completely."
msgstr "روی اطلاع‌رسان CENO بزنید تا آن را به‌طور کامل متوقف کنید."

#: intro/qsg.md:block 6 (paragraph)
msgid "Detailed installation instructions are [here](../browser/install.md)."
msgstr "راهنمای ریزبه‌ریز نصب را در [این‌جا](../browser/install.md) می‌یابید."

#: intro/qsg.md:block 7 (header)
msgid "Configuration"
msgstr "تنظیم"

#: intro/qsg.md:block 8 (paragraph)
msgid ""
"Ceno Browser should work out-of-the-box. You can find some [diagnostics and "
"settings](../browser/settings.md) under the *Ceno* menu entry."
msgstr ""
"مرورگر CENO باید به محض نصب به‌خوبی کار کند. شما می‌توانید بعضی [تنظیمات و "
"عیب‌یابی‌ها](browser/settings.md/..) را ذیل عنوان *CENO* در منو بیابید."

#: intro/qsg.md:block 9 (paragraph)
msgid ""
"If you want to make sure that your app is also helping others access blocked "
"content, please [read this section](../browser/bridging.md)."
msgstr ""
"اگر می‌خواهید مطمئن شوید که اپ شما به دیگران نیز برای دسترسی به محتوای "
"مسدودشده کمک می‌کند، لطفاً [این بخش را بخوانید](browser/bridging.md/..)."

#: intro/qsg.md:block 10 (header)
msgid "More questions?"
msgstr "پرسش‌های بیشتر؟"

#: intro/qsg.md:block 11 (unordered list)
msgid "Please see the [FAQ](faq.md)."
msgstr "لطفاً به [پرسش‌های متداول](faq.md) بنگرید."

#: intro/qsg.md:block 11 (unordered list)
msgid "Refer to the [troubleshooting guide](../browser/troubleshooting.md)."
msgstr ""
"به [راهنمای شناسایی و رفع اشکال](browser/troubleshooting.md/..) رجوع کنید."

#: intro/qsg.md:block 11 (unordered list)
msgid ""
"Contact us by writing to [cenoers@equalitie.org](mailto:cenoers@equalitie."
"org)."
msgstr ""
"با ما با نشانی [cenoers@equalitie.org](mailto:cenoers@equalitie.org) تماس "
"بگیرید."

#: intro/qsg.md:block 11 (unordered list)
msgid ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.Ceno"
msgstr ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno"

#: intro/qsg.md:block 11 (unordered list)
msgid "[ceno-gh]: https://github.com/censorship-no/Ceno-browser/releases"
msgstr "[ceno-gh]: https://gitlab.com/censorship-no/ceno-browser/releases"

#: intro/qsg.md:block 11 (unordered list)
msgid "[ceno-pask]: https://paskoocheh.com/tools/124/android.html"
msgstr "[ceno-pask]: https://paskoocheh.com/tools/124/android.html"

#: intro/qsg.md:block 11 (unordered list)
msgid ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno"
msgstr ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno"

#: intro/qsg.md:block 11 (unordered list)
msgid "[ceno-gh]: https://gitlab.com/censorship-no/ceno-browser/releases"
msgstr "[ceno-gh]: https://gitlab.com/censorship-no/ceno-browser/releases"

#~ msgid ""
#~ "Run it and press *NEXT* on the introductory screens, then press *START "
#~ "BROWSING*."
#~ msgstr ""
#~ "آن را اجرا کنید و در صفحات ابتدایی دکمه‌ی «بعدی» (NEXT) و سپس دکمه‌ی «شروع "
#~ "مرور» (START BROWSING) را بفشارید."

#~ msgid ""
#~ "Install the CENO Browser from [Google Play](https://play.google.com/store/"
#~ "apps/details?id=ie.equalit.ceno), [GitHub](https://github.com/censorship-"
#~ "no/ceno-browser/releases) or [Paskoocheh](https://paskoocheh.com/"
#~ "tools/124/android.html). *No special permissions are needed*."
#~ msgstr ""
#~ "مرورگر سنو را از [گوگل پلی](https://play.google.com/store/apps/details?"
#~ "id=ie.equalit.ceno)، [گیت‌هاب](https://github.com/censorship-no/ceno-"
#~ "browser/releases) یا [پس‌کوچه](https://paskoocheh.com/tools/124/android."
#~ "html) دریافت و نصب کنید. *به هیچ جواز خاصی نیازی نیست*."

#, fuzzy
#~| msgid "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"
#~ msgid "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"
#~ msgstr "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"
