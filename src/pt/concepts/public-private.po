#
msgid ""
msgstr ""
"PO-Revision-Date: 2023-06-26 18:50+0000\n"
"Last-Translator: ssantos <ssantos@web.de>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/censorship-no/"
"public-private/pt/>\n"
"Language: pt\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.18.1\n"

#: concepts/public-private.md:block 1 (header)
msgid "Public vs. private browsing"
msgstr "Navegação pública versus privativa"

#: concepts/public-private.md:block 2 (paragraph)
msgid ""
"Because of the many techniques used to overcome connectivity issues, Ceno "
"may become a convenient way for you to get all kinds of Web content. And, as "
"you may have already read in previous sections, whenever you retrieve and "
"seed a page using Ceno Browser, it becomes available to others. There may be "
"some content, however, that you do not wish to share (or you do not want to "
"let others know that you are trying to or did retrieve), and fortunately "
"Ceno can help you in this instance as well."
msgstr ""
"Devido às muitas técnicas usadas para superar problemas de conectividade, o "
"Ceno pode se tornar uma forma conveniente de obter vários tipos de conteúdo "
"Web. E, como pode já ter lido em secções anteriores, a qualquer momento em "
"que recupera e semeia uma página usando o Navegador Ceno, ela fica "
"disponível para outras pessoas. Pode haver, contudo, algum conteúdo que não "
"deseja partilhar (ou não quer deixar outras pessoas saberem que está "
"tentando recuperar ou recuperou). Felizmente, o Ceno também pode o ajudar "
"nesse aspeto."

#: concepts/public-private.md:block 3 (paragraph)
msgid ""
"The default mode when you launch the application is **public browsing**. In "
"it, Ceno accesses Web content as described previously:"
msgstr ""
"O modo padrão quando abre a aplicação é **navegação pública**. Nele, o Ceno "
"acede conteúdo Web da forma descrita anteriormente:"

#: concepts/public-private.md:block 4 (ordered list)
msgid "Direct access is attempted."
msgstr "Tenta-se o acesso direto."

#: concepts/public-private.md:block 4 (ordered list)
msgid "Failing that, the distributed cache is searched."
msgstr "Em caso de falha, procura-se o cache distribuído."

#: concepts/public-private.md:block 4 (ordered list)
msgid ""
"Failing that, the content is requested via an injector (maybe via another "
"client)."
msgstr ""
"Em caso de falha, o conteúdo é requisitado através de um injetor (talvez via "
"outro cliente)."

#: concepts/public-private.md:block 5 (paragraph)
msgid ""
"Ceno also has a **private browsing** mode. In it, the distributed cache is "
"never searched, and injection is never attempted:"
msgstr ""
"Ceno também tem um modo **navegação privativa**. Nele, o cache distribuído "
"nunca é procurado e nunca se tenta a injeção:"

#: concepts/public-private.md:block 6 (ordered list)
msgid ""
"Failing that, an injector is contacted (maybe via another client) and used "
"*as a normal proxy server*. Note that in this case, neither the injector nor "
"your client updates the distributed cache with your page."
msgstr ""
"Em caso de falha, um injetor é contatado (talvez via outro cliente) e usado *"
"como um servidor proxy normal*. Note que, nesse caso, nem o injetor nem o "
"seu cliente atualizam o cache distribuído com a sua página."

#: concepts/public-private.md:block 7 (paragraph)
msgid ""
"The different behavior results in different characteristics. Thus, in public "
"mode:"
msgstr ""
"O comportamento diferente resulta em características diferentes. Assim, no "
"modo público:"

#: concepts/public-private.md:block 8 (ordered list)
msgid ""
"You have better chances to get Web content, and help others get that content "
"(from you)."
msgstr ""
"Tem mais chances de obter conteúdo Web e ajudar outras pessoas a obter "
"conteúdo (de si)."

#: concepts/public-private.md:block 8 (ordered list)
msgid ""
"Pages with dynamic content (e.g. updated in real time) may break in obvious "
"or subtle ways."
msgstr ""
"Páginas com conteúdo dinâmico (exemplo, atualizadas em tempo real) podem "
"quebrar de maneiras óbvias ou sutis."

#: concepts/public-private.md:block 8 (ordered list)
msgid ""
"Pages requiring authentication do not work (as passwords and cookies are "
"removed by the client)."
msgstr ""
"Páginas que requerem autenticação não funcionam (pois palavras-passe e "
"cookies são removidos pelo cliente)."

#: concepts/public-private.md:block 8 (ordered list)
msgid ""
"Some browsing activity may be leaked to other users (see [risks](risks.md))."
msgstr ""
"Algumas atividades de navegação podem ser vazadas para outros utilizadores ("
"veja os [riscos](risks.md))."

#: concepts/public-private.md:block 8 (ordered list)
msgid ""
"Some browsing activity may be leaked to injectors (see [risks](risks.md))."
msgstr ""
"Algumas atividades de navegação podem ser vazadas para injetores (veja os "
"[riscos](risks.md))."

#: concepts/public-private.md:block 8 (ordered list)
msgid "You need to trust injectors to retrieve and sign Web content."
msgstr "Precisa confiar nos injetores para recuperar e assinar conteúdo Web."

#: concepts/public-private.md:block 9 (paragraph)
msgid "While in private mode:"
msgstr "Enquanto estiver no modo privativo:"

#: concepts/public-private.md:block 10 (ordered list)
msgid ""
"You may not be able to access blocked Web content if international "
"connectivity is too scarce; even if you could, other Ceno users would not "
"get that content from you."
msgstr ""
"Pode não ser capaz de aceder conteúdo Web bloqueado se a conectividade "
"internacional estiver muito escassa. Mesmo que pudesse, outros utilizadores "
"do Ceno não iriam obter esse conteúdo de si."

#: concepts/public-private.md:block 10 (ordered list)
msgid "Pages with dynamic content will probably work."
msgstr "As páginas com conteúdo dinâmico provavelmente funcionarão."

#: concepts/public-private.md:block 10 (ordered list)
msgid ""
"Pages requiring authentication may work (when your connection is protected "
"by HTTPS, the injector does not see your passwords)."
msgstr ""
"Páginas que requerem autenticação podem funcionar (quando a sua conexão está "
"protegida por HTTPS, o injetor não vê as suas palavras-passe)."

#: concepts/public-private.md:block 10 (ordered list)
msgid "Browsing activity is not leaked to other users."
msgstr "A atividade de navegação não é vazada para outros utilizadores."

#: concepts/public-private.md:block 10 (ordered list)
msgid ""
"Limited browsing activity is leaked to injectors (with HTTPS, only the "
"origin server name or address)."
msgstr ""
"Uma quantidade limitada de atividade de navegação é vazada para injetores ("
"com HTTPS, apenas o nome ou endereço do servidor de origem)."

#: concepts/public-private.md:block 10 (ordered list)
msgid ""
"You need not trust injectors (with HTTPS, usual certificate-based security "
"still works)."
msgstr ""
"Não precisa confiar nos injetores (com HTTPS, a segurança usual baseada em "
"certificados ainda funciona)."

#: concepts/public-private.md:block 11 (paragraph)
msgid ""
"In conclusion: if you are using Ceno to read the news, watch videos, browse "
"Wikipedia and other static resources that are otherwise censored in your "
"network, consider using the default *public browsing* mode. And if you want "
"to login to Twitter or edit your WordPress website, use *private browsing* "
"mode."
msgstr ""
"Em conclusão: se usa Ceno para ler notícias, assistir vídeos, navegar pela "
"Wikipédia e por outras fontes estáticas que estão de alguma forma censuradas "
"na sua rede, considere usar o modo padrão *navegação pública*. E se quer "
"fazer login no Twitter ou editar o seu website WordPress, use o modo *"
"navegação privativa*."

#: concepts/public-private.md:block 12 (paragraph)
msgid ""
"Please read the section on [risks](risks.md) for a more detailed "
"explanation. Also note that your client can continue to operate as a bridge "
"and a seeder regardless of public or private browsing. We explain this in "
"greater detail in the [Helping others](../browser/bridging.md) section of "
"the manual."
msgstr ""
"Por favor, leia a secção sobre [riscos](risks.md) para uma explicação mais "
"detalhada. Note também que o seu cliente pode continuar a operar como uma "
"ponte e um semeador independentemente da navegação pública ou privativa. "
"Explicamos isso em detalhes na secção [Ajudando outras pessoas](../browser/"
"bridging.md) do manual."
