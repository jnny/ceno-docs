msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-03 02:50+0200\n"
"PO-Revision-Date: 2023-09-05 19:57+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Burmese <https://hosted.weblate.org/projects/censorship-no/"
"ouinet/my/>\n"
"Language: my\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.0.1-dev\n"

#
#: intro/ouinet.md:block 1 (header)
msgid "What is Ouinet?"
msgstr "Ouinet ဆိုတာ ဘာလဲ။"

#: intro/ouinet.md:block 2 (paragraph)
msgid ""
"[Ouinet][Ouinet] is a core technology that allows Ceno Browser to share Web "
"content with other devices. Ouinet comes in the form of reusable computer "
"code (a library) that an application like Ceno can use to become a "
"participant in a network of cooperating nodes that communicate directly "
"([peer-to-peer][P2P] or P2P) to help access and store new Web content, and "
"to request and deliver previously accessed content to others."
msgstr ""
"[Ouinet][Ouinet] သည် CENO ဘရောက်ဇာကို အခြားစက်များနှင့် ဝက်ဘ်အကြောင်းအရာ မျှဝေခွင့်ပြုသည့် အဓိက "
"နည်းပညာ ဖြစ်သည်။ Ouinet သည် CENO ကဲ့သို့ အက်ပလီကေးရှင်းတစ်ခုက ဝက်ဘ် အကြောင်းအရာအသစ်ကို ကူညီ၍ "
"ဝင်ရောက်သုံးစွဲပြီး သိုလှောင်ရန်နှင့် ယခင်က ဝင်ရောက်သုံးစွဲထားသော အကြောင်းအရာကို တောင်းဆိုပြီး "
"အခြားသူများထံ ပို့ဆောင်ရန်အတွက် တိုက်ရိုက် ဆက်သွယ်သော ပူးပေါင်းဆောင်ရွက်သည့် ဆက်သွယ်ရေးပွိုင့်များ၏ ကွန်ရက် "
"([တစ်ဦးမှတစ်ဦးထံ][P2P] သို့မဟုတ် P2P) တွင် ပါဝင်ဆောင်ရွက်သူ ဖြစ်လာရန် အသုံးပြုနိုင်သော "
"ပြန်လည်အသုံးပြုနိုင်သည့် ကွန်ပျူတာကုဒ် (စာကြည့်တိုက် - library) ပုံစံအနေနှင့် လာသည်။"

#: intro/ouinet.md:block 3 (paragraph)
msgid ""
"Ouinet is based on a clever combination and use of existing technologies to "
"accomplish each of its functionalities: locating other participants is done "
"with techniques coming from the world of file-sharing (BitTorrent's "
"distributed hash table), communicating with them uses common Web and file-"
"sharing protocols (HTTP proxy requests and BitTorrent's µTP), and modern "
"industry standards ensure the security of communications and the "
"authenticity of exchanged content (TLS encryption and Ed25519 signatures). "
"Ouinet allows replacing particular technologies with others if needed (for "
"instance, some uses of µTP can be replaced by Tor's Pluggable Transports)."
msgstr ""
"Ouinet သည် ၎င်း၏ အသုံးဝင်မှုတစ်ခုစီကို ရရှိရန် တည်ရှိပြီးသား နည်းပညာများကို ကောင်းစွာ ပေါင်းစပ်၍ "
"အသုံးပြုမှုအပေါ် အခြေခံသည် - အခြား ပါဝင်ဆောင်ရွက်သူများကို တည်နေရာရှာခြင်းကို ဖိုင်မျှဝေသည့် ကမ္ဘာမှလာသည့် "
"နည်းပညာများဖြင့် ပြုလုပ်သည် (BitTorrent ၏ ဖြန့်ဝေထားသော ဟက်ရှ်ဇယား)၊ ၎င်းတို့နှင့် ဆက်သွယ်ရာတွင် ဘုံ "
"ဝက်ဘ်နှင့် ဖိုင်မျှဝေခြင်းဆိုင်ရာ ပရိုတိုကောများကို အသုံးပြုသည် (HTTP ပရောက်စီ တောင်းဆိုမှုများနှင့် "
"BitTorrent ၏ µTP)၊ ထို့ပြင် ခေတ်မီ လုပ်ငန်း စံနှုန်းများက ဆက်သွယ်မှုများ၏ လုံခြုံရေးနှင့် ဖလှယ်ထားသော "
"အကြောင်းအရာ၏ စစ်မှန်မှုကို သေချာစေသည် (TLS ကုဒ်ဖြင့် ပြောင်းလဲမှုနှင့် Ed25519 ကိုယ်ပိုင်လက္ခဏာများ)။ "
"Ouinet သည် လိုအပ်ပါက အချို့သော နည်းပညာများကို အခြားနည်းပညာများဖြင့် အစားထိုးခွင့်ပြုသည် (ဥပမာ - "
"µTP ၏ အချို့ အသုံးပြုမှုများကို Tor ၏ Pluggable Transports ဖြင့် အစားထိုးနိုင်သည်)။"

#: intro/ouinet.md:block 4 (paragraph)
msgid ""
"On mobile devices, Ouinet can be embedded into end-user applications (as an "
"Android library). In computers, it can be used by normal Web clients like "
"browsers (as a local HTTP proxy)."
msgstr ""
"မိုဘိုင်းစက်များတွင် Ouinet ကို အသုံးပြုသူ အက်ပလီကေးရှင်းများထဲသို့ (Android စာကြည့်တိုက်အဖြစ်) "
"ထည့်သွင်းနိုင်သည်။ ကွန်ပျူတာများတွင် ၎င်းကို ဘရောက်ဇာများကဲ့သို့ ပုံမှန် ဝက်ဘ်လက်ခံစက်များက (စက်တွင်း HTTP "
"ပရောက်စီ) အဖြစ် အသုံးပြုနိုင်သည်။"

#: intro/ouinet.md:block 5 (paragraph)
msgid ""
"Same as Ceno Browser, Ouinet is developed by [eQualitie][eQualitie] as Free/"
"Libre/Open-Source software."
msgstr ""
"CENO ဘရောက်ဇာကဲ့သို့ပင် Ouinet ကို [eQualitie][eQualitie] က အခမဲ့/လွတ်လပ်သော/ကန့်သတ်မထားသော "
"ရင်းမြစ် ဆော့ဖ်ဝဲအဖြစ် တီထွင်ဖန်တီးသည်။"

#: intro/ouinet.md:block 6 (header)
msgid "Who is it for?"
msgstr "ဘယ်သူ့အတွက်လဲ။"

#: intro/ouinet.md:block 7 (paragraph)
msgid ""
"Ouinet is mostly useful for software developers, content creators and "
"publishers who want to enable users of their applications to share retrieved "
"content with other users. This reduces the overall demand on the application "
"server and improves content accessibility for users living in countries that "
"block access to that server."
msgstr ""
"Ouinet သည် ၎င်းတို့၏ အက်ပလီကေးရှင်းကို သုံးစွဲသူများအား ပြန်ထုတ်ထားသည့် အကြောင်းအရာကို "
"အခြားသုံးစွဲသူများနှင့် မျှဝေခွင့်ပြုလိုသည့် ဆော့ဖ်ဝဲ ရေးသားသူများ၊ အကြောင်းအရာ ဖန်တီးသူများနှင့် "
"ထုတ်ဝေသူများအတွက် အဓိက အသုံးဝင်သည်။ ယင်းသည် အက်ပလီကေးရှင်း ဆာဗာအား ယေဘုယျ တောင်းဆိုမှုကို "
"လျှော့ချပေးပြီး ထိုဆာဗာကို ဝင်ရောက်သုံးစွဲခွင့် ပိတ်ဆို့ထားသော နိုင်ငံများတွင် နေထိုင်သည့် သုံးစွဲသူများအတွက် "
"အကြောင်းအရာ ဝင်ရောက်သုံးစွဲနိုင်မှုကို တိုးတက်စေသည်။"

#: intro/ouinet.md:block 8 (paragraph)
msgid ""
"Please note that Ouinet is an evolving experimental project: some features "
"may not work reliably enough in certain scenarios, bugs may exist and "
"crashes may occur. We encourage you to reach out to us at [cenoers@equalitie."
"org](mailto:cenoers@equalitie.org), test it and report back - your feedback "
"is very welcome!"
msgstr ""
"Ouinet သည် စမ်းသပ်ဆဲ ပ‌ရောဂျက်ဖြစ်ကြောင်း ကျေးဇူးပြု၍ သတိပြုပါ - အချို့သော "
"အခြေအနေများတွင် လုပ်ဆောင်ချက်အချို့ကို လုံလောက်စွာ အားမထားရဘဲ "
"အလုပ်လုပ်နိုင်သည်၊ စက်ချွတ်ယွင်းချက်များ ရှိနိုင်ပြီး ရပ်တန့်မှုများ "
"ဖြစ်နိုင်သည်။ ကျွန်ုပ်တို့ထံ [cenoers@equalitie.org](mailto:cenoers@equalitie"
".org) သို့ ဆက်သွယ်ပြီး ၎င်းကို စမ်းသပ်ကာ ပြန်လည်အစီရင်ခံရန် သင့်အား "
"ကျွန်ုပ်တို့ အားပေးပါသည် - သင့် အကြောင်းပြန်မှုကို ကြိုဆိုပါသည်။"

#: intro/ouinet.md:block 9 (quote)
msgid ""
"**Warning:** *Ouinet is not an anonymity tool*. If you are unsure about its "
"adequacy for a certain task, do not hesitate to contact us."
msgstr ""
"**သတိပေးချက်-** *Ouinet သည် အမည်လျှို့ဝှက်ပေးသော ကိရိယာ မဟုတ်ပါ*။ တိကျသော အလုပ်တစ်ခုအတွက် ၎င်း၏ "
"သင့်လျော်မှုနှင့် ပတ်သက်ပြီး မသေချာပါက ကျွန်ုပ်တို့ကို ဆက်သွယ်ရန် မတွန့်ဆုတ်ပါနှင့်။"

#: intro/ouinet.md:block 9 (quote)
msgid "[Ouinet]: https://github.com/equalitie/ouinet/"
msgstr "[Ouinet]: https://github.com/equalitie/ouinet/"

#: intro/ouinet.md:block 9 (quote)
msgid "[P2P]: https://en.wikipedia.org/wiki/Peer-to-peer"
msgstr "[P2P]: https://en.wikipedia.org/wiki/Peer-to-peer"

#: intro/ouinet.md:block 9 (quote)
msgid "[eQualitie]: https://equalit.ie/"
msgstr "[eQualitie]: https://equalit.ie/"
