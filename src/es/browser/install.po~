msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-05 20:50+0200\n"
"PO-Revision-Date: 2023-09-05 22:20+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Spanish <https://hosted.weblate.org/projects/censorship-no/"
"browser-install/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.0.1-dev\n"

#
#: browser/install.md:block 1 (header)
msgid "Installing Ceno"
msgstr "Instalando Ceno"

#: browser/install.md:block 2 (paragraph)
msgid "Ceno Browser can be installed via the following means:"
msgstr "Navegador Ceno puede ser instalado a través de los siguientes medios:"

#: browser/install.md:block 3 (unordered list)
msgid ""
"[Google Play][ceno-gplay] (*Ceno Browser* from *eQualitie*): the recommended "
"source for most Android users."
msgstr ""
"[Google Play][ceno-gplay] (*Navegador Ceno*, de *eQualitie*): la fuente "
"recomendada para la mayoría de usuaria(o)s de Android."

#: browser/install.md:block 3 (unordered list)
msgid "[GitHub][ceno-gh]: for Android devices without Google Play."
msgstr "[GitHub][ceno-gh]: para dispositivos Android sin Google Play."

#: browser/install.md:block 3 (unordered list)
msgid ""
"[Paskoocheh][ceno-pask]: for users in countries blocking access to the "
"previous channels."
msgstr ""
"[Paskoocheh][ceno-pask]: para usuaria(o)s en países que bloquean el acceso a "
"los canales previos."

#: browser/install.md:block 4 (paragraph)
msgid ""
"Ceno requires an ARM64 or Neon-capable ARM32 device running at least Android "
"4.1 Jelly Bean, thus most mobile devices released from late 2012 onward "
"should work. Ceno needs *no special permissions* to run."
msgstr ""
"Ceno requiere un dispositivo basado en ARM64 o ARM32 compatible con Neo que "
"ejecute al menos Android 4.1 Jelly Bean, por lo que la mayoría de "
"dispositivos móviles posteriores a finales de 2012 deberían funcionar. Ceno "
"*no requiere permisos especiales* para ejecutarse."

#: browser/install.md:block 5 (quote)
msgid ""
"**Warning:** Please be *extremely skeptical* about installing Ceno Browser "
"from sources other than the ones listed above. Because of the application's "
"nature, their potential users may become a target for all kinds of fake or "
"manipulated versions used to violate user privacy or attack other Ceno and "
"Ouinet users. If in doubt, please contact [cenoers@equalitie.org](mailto:"
"cenoers@equalitie.org) before installing a suspicious app."
msgstr ""
"**Advertencia:** Por favor sé *extremadamente escéptica(o)* al instalar el "
"Ceno Browser desde otras fuentes que no sean las listadas arriba. Debido a "
"la naturaleza de la aplicación, sus usuaria(o)s potenciales podrían tornarse "
"en el blanco de toda clase de versiones falsas o manipuladas usadas para "
"violar la privacidad del usuaria(o) o atacar a otra(o)s usuaria(o)s de Ceno "
"y Ouinet. Ante la duda, por favor contacta a [cenoers@equalitie.org](mailto:"
"cenoers@equalitie.org) antes de instalar una app sospechosa."

#: browser/install.md:block 6 (header)
msgid "Stopping Ceno completely"
msgstr "Deteniendo Ceno completamente"

#: browser/install.md:block 7 (paragraph)
msgid ""
"Every time you start the app, a Ceno icon will appear on your device's "
"notification bar. This icon represents the *Ceno Browser service*, which is "
"the part of Ceno that runs permanently (even when you are not browsing) and "
"allows other clients to use your device as a bridge and retrieve content "
"from it at any time."
msgstr ""
"Cada vez que inicies la app, un icono de Ceno aparecerá en la barra de "
"notificaciones de tu dispositivo. Este icono representa al *servicio del "
"Navegador Ceno*, que es la parte de Ceno que se ejecuta permanentemente (aún "
"cuando no estés navegando) y permite a otra(o)s clientes usar tu dispositivo "
"como puente y descargar contenido desde él en cualquier momento."

#: browser/install.md:block 8 (paragraph)
msgid ""
"Since running such service uses network and processor resources, you may "
"want to stop it whenever you are on the move (i.e. not connected to Wi-Fi or "
"far from a charger). Tapping on the notification attached to the icon will "
"stop both Ceno and its service at once (until you open Ceno again)."
msgstr ""
"Ya que ejecutar tal servicio usa recursos de red y procesador, podrías "
"querer detenerlo cada vez que estés desplazándote (esto es, no conectado al "
"Wi-Fi o lejos de un cargador). Pulsando sobre la notificación adjunta al "
"icono detendrá tanto Ceno como a su servicio al mismo tiempo (hasta que "
"abras Ceno de nuevo)."

#: browser/install.md:block 9 (paragraph)
msgid ""
"![Figure: Tap on the notification to stop the Ceno service](images/tap-stop."
"png)"
msgstr ""
"![Figura: Pulsar sobre la notificación para detener el servicio Ceno](images/"
"tap-stop.png)"

#: browser/install.md:block 10 (header)
msgid "Purging all Ceno data (the \"panic button\")"
msgstr "Purgando todos los datos en Ceno (el \"botón de pánico\")"

#: browser/install.md:block 11 (paragraph)
msgid ""
"The *Ceno Browser service* notification shown above includes a few "
"accompanying actions which can be triggered by tapping on them. The *Home* "
"action will just open Ceno with a new public browsing tab showing its home "
"page. The *Erase* action demands more explanation."
msgstr ""
"La notificación *servicio Navegador Ceno* mostrada arriba incluye unas pocas "
"acciones de acompañamiento que pueden ser desencadenadas pulsando sobre "
"ellas. La acción *Inicio* solo abrirá a Ceno con una nueva pestaña de "
"navegación pública mostrando su página de inicio. La acción *Borrar* demanda "
"más explicación."

#: browser/install.md:block 12 (quote)
msgid ""
"**Note:** If the actions below the notification are not visible, drag the "
"notification from its center towards the bottom to unfold it. If you can "
"only see the *Home* action, your device may be too old to support the "
"*Erase* action."
msgstr ""
"**Nota:** Si las acciones debajo de la notificación no son visibles, "
"arrastra la notificación desde su centro hacia el fondo para desplegarla. Si "
"sólo aparece la acción *Inicio*, puede que tu dispositivo sea demasiado "
"antiguo para permitir la acción *Borrar*."

#: browser/install.md:block 13 (paragraph)
msgid ""
"Shall you ever need to quickly stop Ceno and clear absolutely all data "
"related to it (not only cached content, but also settings like favorites, "
"passwords and all browsing history), you can tap on *Erase*. To avoid losing "
"your data accidentally, this will not remove anything yet, but just show an "
"additional action for a brief moment, as pictured below:"
msgstr ""
"Si alguna vez necesitaras detener Ceno rápidamente y limpiar absolutamente "
"todos los datos relacionados con él (no solamente el contenido cacheado, "
"sino también configuraciones como favoritos, contraseñas y todo el historial "
"de navegación), puedes pulsar sobre *Borrar*. Para evitar perder tus datos "
"accidentalmente, esto aún no eliminará nada, solo te mostrará una acción "
"adicional por un breve momento, como se ilustra abajo:"

#: browser/install.md:block 14 (paragraph)
msgid ""
"![Figure: The last action stops Ceno and clears all its data](images/tap-"
"purge.png)"
msgstr ""
"![Figura: La última acción detiene Ceno y limpia todos sus datos](images/tap-"
"purge.png)"

#: browser/install.md:block 15 (paragraph)
msgid ""
"If you tap on the *Yes* action, Ceno will be stopped and all its data "
"removed *without further questions*, effectively leaving your device as if "
"Ceno had never been used."
msgstr ""
"Si pulsas sobre la acción *Sí*, Ceno será detenido y todos sus datos "
"removidos *sin más preguntas*, dejando efectivamente tu dispositivo como si "
"Ceno nunca hubiese sido usado."

#: browser/install.md:block 16 (paragraph)
msgid "If you do not tap on the action, it will go away in a few seconds."
msgstr "Si no pulsas sobre la acción, desaparecerá en unos pocos segundos."

#: browser/install.md:block 17 (quote)
msgid ""
"**Note:** The method described above requires that Ceno be running on your "
"device. To accomplish the same effect when Ceno is stopped, you can use "
"Android's general *Settings* page and, under the *Apps* entry, choose Ceno "
"and then *Clear data*."
msgstr ""
"**Nota:** El método descrito arriba requiere que Ceno esté ejecutándose en "
"tu dispositivo. Para lograr el mismo efecto cuando Ceno está detenido, "
"puedes usar la página general de *Ajustes* de Android y, bajo la entrada "
"*Apps*, elegir Ceno y luego *Limpiar datos*."

#: browser/install.md:block 17 (quote)
msgid "As a harsher alternative, you may completely uninstall the app."
msgstr ""
"Como alternativa más drástica, podrías desinstalar la app completamente."

#: browser/install.md:block 18 (quote)
msgid ""
"**Warning:** Android may still keep other traces of having used an app "
"besides its data, for instance in its system log."
msgstr ""
"**Advertencia:** Android aún podría mantener otros rastros de haber usado "
"una app además de sus datos, por ejemplo en su registro del sistema."

#: browser/install.md:block 18 (quote)
msgid ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.Ceno"
msgstr ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno"

#: browser/install.md:block 18 (quote)
msgid "[ceno-gh]: https://github.com/censorship-no/Ceno-browser/releases"
msgstr "[ceno-gh]: https://gitlab.com/censorship-no/ceno-browser/releases"

#: browser/install.md:block 18 (quote)
msgid "[ceno-pask]: https://paskoocheh.com/tools/124/android.html"
msgstr "[ceno-pask]: https://paskoocheh.com/tools/124/android.html"

#: browser/install.md:block 18 (quote)
msgid ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno"
msgstr ""
"[ceno-gplay]: https://play.google.com/store/apps/details?id=ie.equalit.ceno"

#: browser/install.md:block 18 (quote)
msgid "[ceno-gh]: https://gitlab.com/censorship-no/ceno-browser/releases"
msgstr "[ceno-gh]: https://gitlab.com/censorship-no/ceno-browser/releases"

#: browser/install.md:block 3 (unordered list)
msgid "[Gitlab][ceno-gh]: for Android devices without Google Play."
msgstr "[Gitlab][ceno-gh]: para dispositivos Android sin Google Play."

#: browser/install.md:block 18 (quote)
msgid "[ceno-gh]: https://gitlab.com/censorship-no/ceno-browser/-/releases"
msgstr "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"

#~ msgid "CENO requires *no special permissions* to run."
#~ msgstr "CENO *no requiere permisos especiales* para ejecutarse."

#~ msgid ""
#~ "When you run CENO for the first time, you will be presented with a series "
#~ "of screens introducing some features generic to Firefox browsers. Just "
#~ "scroll through them by pressing *NEXT* until you see a *Sign in to Sync* "
#~ "button and a *START BROWSING* link. Firefox Sync has not been tested to "
#~ "work with CENO, so just push *START BROWSING*."
#~ msgstr ""
#~ "Cuando ejecutas CENO por primera vez, te será presentada una serie de "
#~ "pantallas introduciendo algunas características genéricas de los "
#~ "navegadores Firefox. Solo pásalas presionando *SIGUIENTE* hasta que veas "
#~ "un botón *Registrarse en Sync* y un vínculo *INICIAR NAVEGACIÓN*. Firefox "
#~ "Sync no ha sido probado para funcionar con CENO, por lo que solo presiona "
#~ "*INICIAR NAVEGACIÓN*."

#, fuzzy
#~ msgid "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"
#~ msgstr "[ceno-gh]: https://github.com/censorship-no/ceno-browser/releases"
