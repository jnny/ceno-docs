msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-05 20:50+0200\n"
"PO-Revision-Date: 2023-05-17 03:01+0000\n"
"Last-Translator: Jenny Ryan <jenny@equalitie.org>\n"
"Language-Team: Chinese (Simplified) <https://hosted.weblate.org/projects/"
"censorship-no/browser-bridging/zh_Hans/>\n"
"Language: zh_Hans\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.18-dev\n"

#
#: browser/bridging.md:block 1 (header)
msgid "Helping other Ceno users browse the Web"
msgstr "帮助其他Ceno用户浏览互联网"

#: browser/bridging.md:block 2 (paragraph)
msgid ""
"A peer-to-peer network is built from every node connected to it (yes, that "
"means you as well!). The more nodes, the stronger and more versatile the "
"network becomes. If you are running Ceno Browser from a country that does "
"not censor the Internet (or not as heavily as some), consider helping other "
"Ceno users by becoming a **bridge** node. You will then begin to route "
"traffic between clients living in heavily censored countries and Ceno "
"injectors. You will not be able to see their traffic (it will be sent "
"through an encrypted tunnel), nor will any of this traffic remain on your "
"device."
msgstr ""
"一个点对点的网络是由连接到它的每个节点（也包括你！）建立的。节点越多，网络就"
"越强大，功能也越多。如果你在一个没有互联网审查的国家（或者不像某些国家那样严"
"重）运行Ceno浏览器，可以考虑通过成为一个**桥梁**节点来帮助其他Ceno用户。然后"
"你将开始，在生活在严重审查的国家的客户和Ceno注入器之间，进行流量的路由。你将"
"无法看到他们的流量（它将通过加密的隧道发送），也不会有任何这种流量留在你的设"
"备上。"

#: browser/bridging.md:block 3 (quote)
msgid ""
"**Note:** The configuration described in this section may also help your "
"device to effectively seed content to others on the distributed cache, so "
"please consider applying it as well when using Ceno in a censoring country "
"(but keep in mind the [risks](../concepts/risks.md) of serving such content "
"to others)."
msgstr ""
"**注意**本节所描述的配置，也可以帮助你的设备有效地将内容播种给分布式缓存上的"
"其他人，因此，您在有进行审查的国家使用Ceno时，也请考虑应用它（但请记住向他人"
"提供此类内容的[风险](.../concepts/risks.md)）。"

#: browser/bridging.md:block 4 (header)
msgid "How to become a Ceno bridge"
msgstr "如何成为Ceno的网桥"

#: browser/bridging.md:block 5 (paragraph)
msgid ""
"This functionality is already built into Ceno Browser. Your device will need "
"to be connected to a Wi-Fi network that has either UPnP enabled or explicit "
"port forwarding configured for Ceno. See the next sections for further "
"details."
msgstr ""
"这一功能已经内置于Ceno浏览器中。您的设备将需要连接到一个Wi-Fi网络，并将此网络"
"为Ceno配置UPnP功能或显式端口转发。更多细节请看下面的章节。"

#: browser/bridging.md:block 6 (paragraph)
msgid ""
"However, please note that Android will only allow a mobile device to act as "
"a proper bridge while you are actively using it, as power-saving features "
"will throttle the operation of Ceno otherwise."
msgstr ""
"然而，请注意，安卓系统只允许移动设备在您使用它时充当可用的桥梁。在不使用设备"
"时，省电功能将扼杀Ceno的运作。"

#: browser/bridging.md:block 7 (quote)
msgid ""
"**Technical note:** This is mainly due to Android's [Doze mode](https://"
"developer.android.com/training/monitoring-device-state/doze-standby "
"\"Android Developers – Optimize for Doze and App Standby\") slowing down the "
"operation of the native Ouinet library. Unfortunately, disabling battery "
"optimization for Ceno does not seem to exclude Ouinet from it. Your "
"particular device may also include its own power-saving features which may "
"interfere with Ceno; please check [Don't kill my app!][Don't kill my app!] "
"for your device's brand."
msgstr ""

#: browser/bridging.md:block 8 (paragraph)
msgid ""
"Thus if you intend to have Ceno acting as a permanent, always-reachable "
"bridge, besides a properly configured Wi-Fi network you will need to:"
msgstr ""
"因此，如果您打算让Ceno充当一个永久的、一直可以到达的网桥，除了一个正确配置的"
"Wi-Fi网络，您还需要："

#: browser/bridging.md:block 9 (ordered list)
msgid "Have your device plugged in to power at all times."
msgstr "将您的设备一直插着电源。"

#: browser/bridging.md:block 9 (ordered list)
msgid "Have the device's screen on at all times."
msgstr "始终保持设备的屏幕开启。"

#: browser/bridging.md:block 9 (ordered list)
msgid ""
"One convenient way of doing this without much power consumption and "
"obnoxious, permanent lighting is using Android's screen saver: enable it "
"under *Settings / Display / Screen saver* (or *Daydream* in some versions), "
"pick the *Clock* widget, choose *When to start screen saver* in the menu and "
"select *While charging* or *Either*. A very dimmed down clock will appear on "
"a black background while the device is not active."
msgstr ""
"实现此事的一个方便的方法——在不用消耗过多电量，也不用让人厌烦的永久亮屏的前提"
"下——是使用安卓的屏幕保护程序：在*设置/显示/屏幕保护程序*（在某些版本是"
"*Daydream*）下启用它，选择*时钟*小部件，在菜单中选择*何时启动屏幕保护程序*，"
"并选择*充电时*或*任一*。这样当设备未被激活时，一个非常暗淡的时钟将出现在黑色"
"背景上。"

#: browser/bridging.md:block 9 (ordered list)
msgid ""
"Please note that you should not use the power button to lock the device as "
"this will turn the screen off. Instead, just wait for the device to lock "
"itself with the screen on."
msgstr ""
"请注意，不要使用电源按钮来锁定设备，因为这将关闭屏幕。只需在屏幕开启时等待，"
"让设备自行锁定。"

#: browser/bridging.md:block 10 (paragraph)
msgid ""
"If that setup is not an option for you, do not desist yet! If you have a "
"computer with good connectivity that stays on most of the time, please "
"continue reading."
msgstr ""

#: browser/bridging.md:block 11 (header)
msgid "Running a bridge on a computer"
msgstr ""

#: browser/bridging.md:block 12 (paragraph)
msgid ""
"If your computer supports [Docker containers][docker], you can run a pre-"
"configured Ceno client on it to act as a bridge. If Docker is not yet "
"installed, please follow the instructions to [install the Docker Engine]"
"[docker-install] in your platform. For Debian derivatives like Ubuntu or "
"Linux Mint, you can just run: `sudo apt install docker.io`"
msgstr ""

#: browser/bridging.md:block 13 (paragraph)
msgid ""
"To deploy a Ceno client container you only need to run the following command "
"on a terminal (it looks scary but you can just copy and paste it as is on "
"the command line):"
msgstr ""

#: browser/bridging.md:block 15 (paragraph)
msgid ""
"If your computer is not based on GNU/Linux, the command needs to be slightly "
"different:"
msgstr ""

#: browser/bridging.md:block 17 (paragraph)
msgid ""
"The command will start a container named `ceno-client` that will run on "
"every boot unless you explicitly tell it to stop. Please check the [Ceno "
"Docker client documentation][ceno-client-doc] for more information on how to "
"manipulate the container."
msgstr ""

#: browser/bridging.md:block 18 (quote)
msgid ""
"**Note:** This client has no *Ceno Settings*: when instructed below to "
"access that page, open instead the [client front-end](../client/front-end."
"md), which contains mostly equivalent information."
msgstr ""

#: browser/bridging.md:block 19 (header)
msgid "Enabling UPnP on your Wi-Fi router"
msgstr "在您的Wi-Fi路由器上启用UPnP"

#: browser/bridging.md:block 20 (paragraph)
msgid ""
"[UPnP][UPnP] is the easiest way of making your Ceno Browser (or computer "
"client) reachable to the Ceno network. The [Ceno Settings](settings.md) page "
"will indicate the UPnP status on your local network."
msgstr ""
"[UPnP][UPnP]是使您的Ceno浏览器可以接触到Ceno网络的最简单方法。在[Ceno设置]"
"(settings.md)页面上会显示您本地网络的UPnP状态。"

#: browser/bridging.md:block 21 (quote)
msgid ""
"**Note:** Enabling UPnP on the Wi-Fi router may expose devices on your "
"network to external interference. Please make yourself [aware of the risks]"
"[upnp-risks] and also consider using alternative methods as explained below."
msgstr ""
"**注意：**在Wi-Fi路由器上启用UPnP，可能会使您网络上的设备受到外部干扰。请[了"
"解其风险][upnp-risks]，同时考虑使用下面解释的其他方法。"

#: browser/bridging.md:block 22 (paragraph)
msgid ""
"A status like the one below indicates that UPnP is not available or not "
"working on your WiFi router:"
msgstr "像上图所示的状态，表明你的WiFi路由器上没有启用UPnP。"

#: browser/bridging.md:block 23 (quote)
msgid "**Reachability status**"
msgstr ""

#: browser/bridging.md:block 23 (quote)
msgid "**UPnP status**"
msgstr ""

#: browser/bridging.md:block 24 (paragraph)
msgid ""
"The status below indicates that UPnP is likely working and Ceno is currently "
"verifying connectivity:"
msgstr "上面的状态表明，UPnP很有可能正在工作，Ceno目前正在验证连通性。"

#: browser/bridging.md:block 26 (paragraph)
msgid ""
"The status below indicates that UPnP is working and you can bridge "
"connections for other Ceno users:"
msgstr "上面的状态表明，UPnP正在工作，你可以为其他Ceno用户进行桥接连接。"

#: browser/bridging.md:block 28 (quote)
msgid ""
"**Note:** Even if UPnP is working, your router may still not be reachable "
"from the outside. This can be the case when *Ceno Settings* reports "
"*External UDP endpoints* which look like [CGNAT][CGNAT] addresses `100.X.Y.Z:"
"N` with X between 64 and 127 (increasingly common among home ISPs), or like "
"private addresses `10.X.Y.Z:N`, `172.X.Y.Z:N` with X between 16 and 31, and "
"`192.168.X.Y:N`. If so, please contact your ISP or network administrator to "
"get a public address on your router or to establish port forwardings to the "
"external endpoint."
msgstr ""

#: browser/bridging.md:block 29 (paragraph)
msgid ""
"There are many Wi-Fi routers on the market and each has their own particular "
"features. Herein a list of some manufacturers' instructions for enabling "
"UPnP:"
msgstr ""
"市场上有许多Wi-Fi路由器，每个都有自己的特定功能。这里列出了一些制造商关于启用"
"UPnP的说明："

#: browser/bridging.md:block 30 (unordered list)
msgid "[Linksys](https://www.linksys.com/us/support-article?articleNum=138290)"
msgstr ""

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[D-Link](https://eu.dlink.com/uk/en/support/faq/routers/wired-routers/di-"
"series/how-do-i-enable-upnp-on-my-router)"
msgstr ""

#: browser/bridging.md:block 30 (unordered list)
msgid "[Huawei](https://consumer.huawei.com/ph/support/content/en-us00275342/)"
msgstr ""

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[Xfinity](https://www.xfinity.com/support/articles/configure-device-"
"discovery-for-wifi)"
msgstr ""

#: browser/bridging.md:block 30 (unordered list)
msgid "[TP-Link](https://community.tp-link.com/us/home/kb/detail/348)"
msgstr ""

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[Netgear](https://kb.netgear.com/24306/How-do-I-enable-Universal-Plug-and-"
"Play-on-my-Nighthawk-router)"
msgstr ""

#: browser/bridging.md:block 30 (unordered list)
msgid ""
"[Ubiquiti](https://www.geekzone.co.nz/forums.asp?"
"forumid=66&topicid=205740&page_no=5#1725168)"
msgstr ""

#: browser/bridging.md:block 31 (header)
msgid "Using port forwarding as an alternative to UPnP"
msgstr "使用端口转发作为UPnP的替代方案"

#: browser/bridging.md:block 32 (paragraph)
msgid ""
"Instead of enabling UPnP on your router, you can create a port forwarding "
"rule to make sure that connections from the Ceno network are forwarded to "
"your device. You will need to login to the router's administrative interface "
"and locate the *port forwarding* option. To see which IP address you need to "
"forward the connections to and the relevant port, open the *Ceno Settings* "
"page and look under the *Local UDP endpoints*."
msgstr ""
"若不在您的路由器上启用UPnP，取而代之的是，您也可以创建一个端口转发规则，以确"
"保来自Ceno网络的连接被转发给您的设备。您将需要登录到路由器的管理界面，并找到*"
"端口转发*选项。要查看您需要将连接转发到哪个IP地址以及相关的端口，请打开*Ceno"
"设置*页面，在*本地UDP端点*下查找。"

#: browser/bridging.md:block 33 (quote)
msgid "**Local UDP endpoints**"
msgstr ""

#: browser/bridging.md:block 34 (paragraph)
msgid ""
"The port forwarding must be for the UDP protocol (not TCP). Ceno chooses a "
"random port on first run and keeps it for subsequent runs, but your device's "
"local network IP address may change from time to time. Thus you should "
"periodically review the *Ceno Settings* page to see that your device is "
"reachable to the Ceno network."
msgstr ""
"端口转发必须是针对UDP协议的（而非TCP）。Ceno在第一次运行时选择一个随机的端"
"口，并在以后的运行中保持这个端口，但你的设备的本地网络IP地址可能会不时地发生"
"变化。因此，你应该定期查看*Ceno设置*页面，以了解你的设备是否可到达Ceno网络。"

#: browser/bridging.md:block 35 (quote)
msgid ""
"**Technical note:** Alternatively, you can make sure that the router always "
"assigns the same IP address to your device (e.g. via a static DHCP lease for "
"the device's MAC address)."
msgstr ""
"**技术说明：**或者，你也可以确保路由器始终为你的设备分配相同的IP地址（例如，"
"通过静态DHCP租赁设备的MAC地址）。"

#: browser/bridging.md:block 35 (quote)
msgid ""
"[Doze mode]: https://developer.android.com/training/monitoring-device-state/"
"doze-standby"
msgstr ""

#: browser/bridging.md:block 35 (quote)
msgid "[Don't kill my app!]: https://dontkillmyapp.com/"
msgstr ""

#: browser/bridging.md:block 35 (quote)
msgid "[docker]: https://en.wikipedia.org/wiki/Docker_(software)"
msgstr ""

#: browser/bridging.md:block 35 (quote)
msgid "[docker-install]: https://docs.docker.com/engine/install/"
msgstr ""

#: browser/bridging.md:block 35 (quote)
msgid ""
"[ceno-client-doc]: https://github.com/censorship-no/ceno-docker-"
"client#running-the-client"
msgstr ""

#: browser/bridging.md:block 35 (quote)
msgid "[UPnP]: https://en.wikipedia.org/wiki/Universal_Plug_and_Play"
msgstr ""

#: browser/bridging.md:block 35 (quote)
msgid ""
"[upnp-risks]: https://www.howtogeek.com/122487/htg-explains-is-upnp-a-"
"security-risk"
msgstr ""

#: browser/bridging.md:block 35 (quote)
msgid "[CGNAT]: https://en.wikipedia.org/wiki/Carrier-grade_NAT"
msgstr ""
